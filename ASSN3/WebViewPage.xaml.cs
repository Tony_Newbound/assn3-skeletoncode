﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASSN3
{
	public partial class WebViewShow : ContentPage
	{
		public WebViewShow(string Url)
		{
			InitializeComponent();

            ShowWeb.Source = Url;
            System.Diagnostics.Debug.WriteLine($"WATCH OUT FOR THIS!!!!{Url}");
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
