﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebUrl
	{
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
		public int Id { get; set; }

		public string Url { get; set; }
		public string Images { get; set; }
		public string Title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{

        private SQLiteAsyncConnection _connection;
        private List<WebUrl> Urls;


		public ASSN3Page()
		{
            


                InitializeComponent();

                _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

            }
           

		async void ReloadPicker()
		{

            await _connection.CreateTableAsync<WebUrl>();
            Urls = await _connection.Table<WebUrl>().ToListAsync();


			if (ShowUrls.Items.Count != 0)
			{
                ShowUrls.Items.Clear();
                
			}
           

			foreach (var x in Urls)
			{
                ShowUrls.Items.Add($"{x.Title}");
			}

		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex == -1)
            {
                await DisplayAlert("Error", "Please select a Url", "OK");
            }
            else
            {
            var passOnUrl = Urls[ShowUrls.SelectedIndex].Url;


            await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
            }

            

		}


		async void AddUrlAction(object sender, System.EventArgs e)
		{
            
            await Navigation.PushModalAsync(new AddUrlPage());

		}

		async void UpdateUrlAction(object sender, System.EventArgs e)
		{
            if(ShowUrls.SelectedIndex == -1 )
            {
                await DisplayAlert("Error", "Please select a Url", "OK");
            }
          else
            {
            Urls = await _connection.Table<WebUrl>().ToListAsync();
            var selectedItems = ShowUrls.SelectedIndex;
            var selectedClass = Urls[ShowUrls.SelectedIndex];
            await Navigation.PushModalAsync(new EditUrlPage(selectedClass , selectedItems));
            }  
                
            

            
        }

        async void RemoveUrl(object sender, System.EventArgs e)
		{
            if (ShowUrls.SelectedIndex == -1)
            {
                await DisplayAlert("Error", "Please select a Url", "OK");
            }
            else
            {
             Urls = await _connection.Table<WebUrl>().ToListAsync();

            var selectedItems = Urls[ShowUrls.SelectedIndex];
            await _connection.DeleteAsync(selectedItems);

            await DisplayAlert("WEB Picker", "URL Addres Removed", "OK");

            ReloadPicker();
            }

            

        }
	}
}